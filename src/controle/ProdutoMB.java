package controle;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import modelo.dao.FornecedorDAO;
import modelo.dao.ProdutoDAO;
import modelo.dominio.Fornecedor;
import modelo.dominio.Produto;

@ManagedBean
@RequestScoped
public class ProdutoMB {

	// DAO'S para fazer a persist�ncia
	private FornecedorDAO daoForn = new FornecedorDAO();
	private ProdutoDAO dao = new ProdutoDAO();

	// Objetos usados nas p�ginas
	private Produto produto = new Produto();
	private List<Produto> produtos;
	private List<Fornecedor> fornecedores;
	
	private String filtroDescricao;

	public String getFiltroDescricao() {
		return filtroDescricao;
	}

	public void setFiltroDescricao(String filtroDescricao) {
		this.filtroDescricao = filtroDescricao;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<Produto> getProdutos() {
		
		if (this.produtos == null)
			this.produtos = this.dao.lerTodos();
		
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public List<Fornecedor> getFornecedores() {
		
		if (this.fornecedores == null)
			this.fornecedores = this.daoForn.lerTodos();
		
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

	// m�todos executados pelos bot�es e links

	public String acaoPesquisar() {
		
		this.produtos = this.dao.filtrarProdutos(this.filtroDescricao);
		
		return "listarProdutos.jsf";
	}

	public void acaoPesquisarAjax(AjaxBehaviorEvent event) {
		
		this.produtos = this.dao.filtrarProdutos(this.filtroDescricao);
	}

	public String acaoListarProdutos() {
		return "listarProdutos.jsf";
	}

	public String acaoAbrirInclusao() {
		
		this.produto = new Produto();
		
		return "editarProduto.jsf";
	}
	
	public String acaoAbrirAlteracao(String codigo)
	{
		// = null;
		Integer id = Integer.parseInt(codigo);
		
		this.produto = this.dao.lerPorId(id);
		
		return "editarProduto.jsf";
	}
	
	public String acaoExcluir(String codigo)
	{
		// = null;
		Integer id = Integer.parseInt(codigo);
		
		this.produto = this.dao.lerPorId(id);
		// excluir o produto
		this.dao.excluir(this.produto);
		
		// limpar os objetos
		this.produto = new Produto();
		this.produtos = null;
		
		return acaoListarProdutos();
	}

	public String acaoSalvarProduto() {
		this.dao.salvar(this.produto);

		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Dados gravados com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, msg);

		return acaoListarProdutos();
	}
}
