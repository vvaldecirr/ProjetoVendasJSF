package modelo.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import modelo.dominio.Produto;

public class ProdutoDAO extends JpaDAO<Produto> {
	
	public List<Produto> filtrarProdutos(String descricao)
	{
		String comando = "from Produto p  where p.descricao like :descricao  order by p.descricao";
		TypedQuery<Produto> query = this.getEntityManager().createQuery(comando, Produto.class);
		query.setParameter("descricao", "%" + descricao + "%");
		
		return query.getResultList();
	}
}
