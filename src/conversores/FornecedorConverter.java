package conversores;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import modelo.dao.FornecedorDAO;
import modelo.dominio.Fornecedor;

@FacesConverter(forClass = Fornecedor.class)
public class FornecedorConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		if (value == null) {
			return null;
		} else {
			Integer id;
			try {
				id = Integer.parseInt(value);
			} catch (NumberFormatException e) {
				return null;
			}

			FornecedorDAO dao = new FornecedorDAO();
			return dao.lerPorId(id);
		}

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		if (value instanceof Fornecedor) {
			Fornecedor forn = (Fornecedor) value;
			return forn.getCodigo().toString();
		}

		return null;
	}

}
